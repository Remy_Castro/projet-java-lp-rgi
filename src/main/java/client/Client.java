package client;

import Objects.Book;
import Objects.Reader;

import java.io.*;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    static private Socket socket;
    static private String address = "localhost";
    static private Integer port = 1234;
    static private ObjectOutputStream os;
    static private ObjectInputStream is;
    static public ArrayList<Book> allBooks = new ArrayList<>();
    static public ArrayList<Reader> allReaders = new ArrayList<>();

    public Client() {
        try {
            socket = new Socket(address, port);

            //Stream init
            os = null;
            is = null;
            try {
                os = new ObjectOutputStream(socket.getOutputStream());
                is = new ObjectInputStream(socket.getInputStream());

                //On charge tous les livres depuis le serveur
                getAllBooks();
            }
            catch(IOException e){
                System.out.println("Erreur IO stream");
                e.printStackTrace();
                try {
                    socket.close();
                } catch (IOException ex) {
                    System.out.println("Erreur fermerture socket");
                    ex.printStackTrace();
                }
                System.exit(-3);
            }
        }
        catch(IOException e){
            System.out.println("Erreur lors de la connexion au serveur");
            e.printStackTrace();
            System.exit(-3);

        }
    }

    static public void getAllBooks(){
        try {
            allBooks = (ArrayList<Book>) is.readObject();
            allReaders = (ArrayList<Reader>) is.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    static public ArrayList<Book> getUserBooks(Integer id){
        ArrayList<Book> books = new ArrayList<>();

        try {
            os.writeUTF("getUserBooks");
            os.writeInt(id);
            os.flush();
            books = (ArrayList<Book>) is.readObject();
        } catch (IOException e) {
            System.out.println("erreur");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return books;
    }

    static public boolean addBook(Book book){
        Boolean result = false;
        Integer id;
        try {
            os.writeUTF("addBook");
            os.writeObject(book);
            os.flush();

            result = is.readBoolean();
            id = is.readInt();
            if(result){
                allBooks.add(new Book(id, book.getTitre(),book.getTome(),book.getAuteur()));
            }
        } catch (IOException e) {
            System.out.println("erreur");
            e.printStackTrace();
        }

        return result;
    }

    static public boolean addReader(Reader reader){
        Boolean result = false;
        Integer id;
        try {
            os.writeUTF("addReader");
            os.writeObject(reader);
            os.flush();

            result = is.readBoolean();
            id = is.readInt();

            if(result){
                allReaders.add(new Reader(id, reader.getLastname(), reader.getFirstname()));
            }

        } catch (IOException e) {
            System.out.println("erreur");
            e.printStackTrace();
        }

        return result;
    }

    static public boolean addReaderBook(Reader reader, Book book){
        Boolean result = false;
        try {
            os.writeUTF("addReaderBook");
            os.writeObject(reader);
            os.writeObject(book);
            os.flush();

            result = is.readBoolean();
        } catch (IOException e) {
            System.out.println("erreur");
            e.printStackTrace();
        }

        return result;
    }

    static public boolean returnBook(Reader reader, Book book){
        Boolean result = false;
        try {
            os.writeUTF("returnBook");
            os.writeObject(reader);
            os.writeObject(book);
            os.flush();

            result = is.readBoolean();
        } catch (IOException e) {
            System.out.println("erreur");
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Déconnexion du serveur
     */
    static public void disconnectFromServer(){
        try {
            os.writeUTF("disconnect");
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


