package Ihm.Library;

import Ihm.Reader.PanelReader;
import Objects.Book;
import Objects.Reader;
import client.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class PanelFormReader extends JPanel {
    private JLabel readerFirstname;
    private JLabel readerLastname;
    private JTextField firstname;
    private JTextField lastname;
    private JButton saveButton;


    public PanelFormReader(){
        this.readerFirstname = new JLabel("Prénom");
        this.readerLastname = new JLabel("Nom");
        this.firstname = new JTextField();
        this.lastname = new JTextField();

        this.saveButton = new JButton("Sauvegarder");

        this.saveButton.setBackground(Color.RED);
        this.saveButton.setForeground(Color.BLACK);


        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(readerLastname);
        add(lastname);
        add(readerFirstname);
        add(firstname);
        add(saveButton);

        this.setListeners();
    }

    private void setListeners(){
        JTextField firstname = this.firstname;
        JTextField lastname = this.lastname;
        this.saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                String firstname_txt = firstname.getText();
                String lastname_txt = lastname.getText();

                if(firstname_txt != "" && lastname_txt != "" ){
                    Boolean result = Client.addReader(new Reader(lastname_txt,firstname_txt));
                    if(result){
                        //Modale qui dit que c'est ajouté
                        System.out.println("Lecteur crée");

                        //Rafraichissement du tableau
                        DefaultTableModel modelTable = (DefaultTableModel) PanelReader.tableAllReaders.getModel();
                        modelTable.addRow(new Object[]{lastname_txt, firstname_txt});
                    }
                }
            }
        });
    }
}
