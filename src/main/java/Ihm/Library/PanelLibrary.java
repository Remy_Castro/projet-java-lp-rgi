package Ihm.Library;

import Ihm.Reader.PanelReader;
import Objects.Book;
import client.Client;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class PanelLibrary extends JPanel {
    private JLabel panelTitle;
    private JLabel labelNameBook;
    private JLabel labelTomeBook;
    private JLabel labelAuthorBook;
    private JTextField bookName;
    private JSpinner tome;
    private JTextField author;
    private JButton saveButton;
    static public JTable tableAllBooks;

    public PanelLibrary(){
        this.panelTitle = new JLabel("Espace libraire");
        this.labelNameBook = new JLabel("Nom du livre");
        this.labelTomeBook = new JLabel("Tome");
        this.labelAuthorBook = new JLabel("Auteur du livre");
        this.bookName = new JTextField();
        this.tome = new JSpinner();
        this.author = new JTextField();
        this.saveButton = new JButton("Enregistrer");

        this.saveButton.setBackground(Color.RED);
        this.saveButton.setForeground(Color.BLACK);

        this.panelTitle.setFont(new Font("Verdana", Font.PLAIN, 20));

        //Modèle de donnée du spinner
        SpinnerModel model = new SpinnerNumberModel(0,0,100,1);
        this.tome.setModel(model);

        //Tableau des livres
        DefaultTableModel modelTable = new DefaultTableModel(){
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelTable.addColumn("Nom");
        modelTable.addColumn("Tome");
        modelTable.addColumn("Auteur");

        //Remplissage tableau de tous les livres
        Client.allBooks.forEach((book) -> {
            modelTable.addRow(new Object[]{book.getTitre(), String.valueOf(book.getTome()),book.getAuteur()});
        });
        this.tableAllBooks = new JTable(modelTable);

        //Stylisation du tableau
        styleArray(tableAllBooks);

        //Ajout des éléments dans le panel
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(panelTitle);
        add(labelNameBook);
        add(bookName);

        add(labelTomeBook);
        add(tome);
        add(labelAuthorBook);
        add(author);
        add(saveButton);

        add(new JLabel("Tous les livres"));
        add(new JScrollPane(tableAllBooks));

        //On ajoute le formulaire de saisie des lecteurs
        add(new PanelFormReader());


        this.setListeners();


        //Changement de couleur du fond et padding de la frame
        setBorder(new EmptyBorder(30, 30, 30, 30));
        setBackground(Color.LIGHT_GRAY);

    }

    /**
     * Définie les écouteurs d'évènement de la classe PanelLibrary
     */
    private void setListeners(){
        JTextField bookName = this.bookName;
        JSpinner tome = this.tome;
        JTextField author = this.author;

        //Création nouveau livre
        this.saveButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mouseClicked(e);
                String title = bookName.getText();
                Integer nb = (Integer) tome.getValue();
                String writer = author.getText();

                if(title != "" && nb > 0 && writer != "" ){
                    Boolean result = Client.addBook(new Book(title,nb,writer));
                    if(result){
                        //Modale qui dit que c'est ajouté
                        System.out.println("Livre crée");

                        //Rafraichissement du tableau
                        DefaultTableModel modelTable = (DefaultTableModel) tableAllBooks.getModel();
                        modelTable.addRow(new Object[]{title, nb,writer});
                    }
                }
            }
        });
    }

    /**
     * Permet de styliser un tableau (Jtable)
     * @param array
     */
    private void styleArray(JTable array){
        array.getTableHeader().setFont(new Font("Segoe UI",Font.BOLD,12));
        array.getTableHeader().setOpaque(false);
        array.getTableHeader().setBackground(new Color(32,136,203));
        array.getTableHeader().setForeground(Color.BLACK);
        array.setRowHeight(25);
    }
}
