package Ihm;
import Ihm.Library.PanelLibrary;
import Ihm.Reader.PanelReader;
import client.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Window extends JFrame {
    //Espace librairie
    private PanelLibrary formSaisie;

    //Espace lecteur
    private PanelReader panelReader;


   public Window(){
       this.formSaisie = new PanelLibrary();
       this.panelReader = new PanelReader();

       setLayout(new FlowLayout());
       add(formSaisie);
       add(Box.createHorizontalStrut(50));
       add(panelReader);


       this.addWindowListener(new WindowAdapter() {
           @Override
           public void windowClosing(WindowEvent e) {
               Client.disconnectFromServer();
               System.out.println("Fermeture de l'application");
               System.exit(1);
           }
       });

       pack();

       this.setMinimumSize(new Dimension(1200,800));
   }

}
