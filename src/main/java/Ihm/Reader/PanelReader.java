package Ihm.Reader;

import Ihm.Library.PanelLibrary;
import Objects.Book;
import Objects.Reader;
import client.Client;
import database.Database;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PanelReader extends JPanel {
    private JLabel panelTitle;
    private JButton returnBookButton;
    static public JTable tableAllReaders;
    static public JTable tableReaderBooks;
    private JButton takeBookButton;


    public PanelReader(){
        this.panelTitle = new JLabel("Espace lecteur");
        this.takeBookButton = new JButton("Prendre ce livre");
        this.returnBookButton = new JButton("Rendre ce livre");

        List<Book> userBooks = new ArrayList<>();

        //Tableau des lecteurs
        DefaultTableModel modelTableReaders = new DefaultTableModel(){
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelTableReaders.addColumn("Nom");
        modelTableReaders.addColumn("Prénom");

        //Remplissage du tableau des lecteurs
        Client.allReaders.forEach((reader) -> {
           modelTableReaders.addRow(new Object[]{reader.getLastname(),reader.getFirstname()});
        });
        tableAllReaders = new JTable(modelTableReaders);

        //Tableau des livres des lecteurs
        DefaultTableModel modelReaderBooks = new DefaultTableModel(){
            public boolean isCellEditable(int row, int col) {
                return false;
            }
        };
        modelReaderBooks.addColumn("Nom");
        modelReaderBooks.addColumn("Tome");
        modelReaderBooks.addColumn("Auteur");
        tableReaderBooks = new JTable(modelReaderBooks);

        this.panelTitle.setFont(new Font("Verdana", Font.PLAIN, 20));

        //Stylisation des tableaux
        styleArray(tableAllReaders);
        styleArray(tableReaderBooks);

        //IHM
        JScrollPane pAllBooks = new JScrollPane(tableAllReaders);
        pAllBooks.setPreferredSize(new Dimension(450,200));

        JScrollPane pReaderBooks = new JScrollPane(tableReaderBooks);
        pReaderBooks.setPreferredSize(new Dimension(450,200));

        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(this.panelTitle);
        add(new JLabel("Les lecteurs"));
        add(pAllBooks);

        add(new JLabel("Ses livres"));
        add(pReaderBooks);
        add(takeBookButton);
        add(returnBookButton);

        //Changement de couleur du fond et padding de la frame
        setBorder(new EmptyBorder(30, 30, 30, 30));
        setBackground(Color.LIGHT_GRAY);

        this.setListeners();
    }

    /**
     * Définie les écouteurs d'évènement de la classe PanelReady
     */
    private void setListeners(){
        //Listeners
        DefaultTableModel modelTableReaderBooks = (DefaultTableModel) tableReaderBooks.getModel();

        //Au changement de la selection dans le tableau
        //Rafraichit le contenu du tableau des lecteurs
        tableAllReaders.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (!e.getValueIsAdjusting()) {
                    //Récupération du lecteur selectionné
                    String lastname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),0).toString();
                    String firstname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),1).toString();

                    DefaultTableModel tableModel = (DefaultTableModel) tableReaderBooks.getModel();

                    //Récupération du lecteur selectionné
                    Optional<Reader> reader_taked = Client.allReaders.stream()
                            .filter(reader -> reader.getLastname() == lastname && reader.getFirstname() == firstname)
                            .findAny();

                    if(reader_taked.isPresent()){
                        tableModel.setRowCount(0); //Réinitialise le contenu du tableau
                        Client.getUserBooks(reader_taked.get().getID()).forEach((book) ->{
                            tableModel.addRow(new Object[]{book.getTitre(), String.valueOf(book.getTome()),book.getAuteur()});
                        });
                    }


                }
            }
        });

        //Reservation d'un livre
        this.takeBookButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //Si une ligne est selectionnée
                if(PanelLibrary.tableAllBooks.getSelectedRow() > -1){
                    //Informations du livre
                    String name = PanelLibrary.tableAllBooks.getValueAt(PanelLibrary.tableAllBooks.getSelectedRow(),0).toString();
                    Integer tome = Integer.parseInt(PanelLibrary.tableAllBooks.getValueAt(PanelLibrary.tableAllBooks.getSelectedRow(),1).toString());
                    String author = PanelLibrary.tableAllBooks.getValueAt(PanelLibrary.tableAllBooks.getSelectedRow(),2).toString();

                    //Informations du lecteur
                    String lastname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),0).toString();
                    String firstname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),1).toString();


                    //On récupère le livre
                    Optional<Book> book_taked = Client.allBooks.stream()
                            .filter(book -> book.getTitre() == name && book.getTome() == tome && book.getAuteur() == author)
                            .findAny();

                    //On récupère le lecteur
                    Optional<Reader> reader_taked = Client.allReaders.stream()
                            .filter(reader -> reader.getLastname() == lastname && reader.getFirstname() == firstname)
                            .findAny();

                    if(book_taked.isPresent() && reader_taked.isPresent()){
                        if(Client.addReaderBook(reader_taked.get(), book_taked.get())){
                            modelTableReaderBooks.addRow(new Object[]{name, tome,author});
                        }
                    }
                }
            }
        });

        //Retour d'un livre à la librairie
        this.returnBookButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                //Si une ligne est selectionnée
                if(tableAllReaders.getSelectedRow() > -1){
                    //Informations du lecteur

                    String lastname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),0).toString();
                    String firstname = tableAllReaders.getValueAt(tableAllReaders.getSelectedRow(),1).toString();


                    //Informations du livre
                    String name = tableReaderBooks.getValueAt(tableReaderBooks.getSelectedRow(),0).toString();
                    Integer tome = Integer.parseInt(tableReaderBooks.getValueAt(tableReaderBooks.getSelectedRow(),1).toString());
                    String author = tableReaderBooks.getValueAt(tableReaderBooks.getSelectedRow(),2).toString();

                    //On récupère le livre

                    Optional<Book> book_returned = Client.allBooks.stream()
                            .filter(book -> book.getTitre() == name && book.getTome() == tome && book.getAuteur() == author)
                            .findAny();

                    //On récupère le lecteur
                    Optional<Reader> reader_taked = Client.allReaders.stream()
                            .filter(reader -> reader.getLastname() == lastname && reader.getFirstname() == firstname)
                            .findAny();

                    if(book_returned.isPresent() && reader_taked.isPresent()){
                        if(Client.returnBook(reader_taked.get(),book_returned.get())){
                            DefaultTableModel modelTable = (DefaultTableModel) PanelReader.tableReaderBooks.getModel();
                            modelTable.removeRow(tableReaderBooks.getSelectedRow());
                        }
                    }
                }
            }
        });
    }

    /**
     * Permet de styliser un tableau (Jtable)
     * @param array
     */
    private void styleArray(JTable array){
        array.getTableHeader().setFont(new Font("Segoe UI",Font.BOLD,12));
        array.getTableHeader().setOpaque(false);
        array.getTableHeader().setBackground(new Color(32,136,203));
        array.getTableHeader().setForeground(Color.BLACK);
        array.setRowHeight(25);
    }

}
