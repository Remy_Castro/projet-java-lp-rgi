package Objects;

import java.io.Serializable;

public class Book implements Serializable {
    private int ID;
    private String titre;
    private int tome;
    private String auteur;


    public Book(int ID, String titre, int tome, String auteur) {
        this.ID = ID;
        this.titre = titre;
        this.tome = tome;
        this.auteur = auteur;
    }

    public Book(String titre, int tome, String auteur) {
        this.ID = 0;
        this.titre = titre;
        this.tome = tome;
        this.auteur = auteur;
    }

    @Override
    public String toString() {
        return "Livre{" +
                "ID=" + ID +
                ", titre='" + titre + '\'' +
                ", tome=" + tome +
                ", auteur='" + auteur + '\'' +
                '}';
    }

    public int getID() {
        return ID;
    }

    public String getTitre() {
        return titre;
    }

    public int getTome() {
        return tome;
    }

    public String getAuteur() {
        return auteur;
    }
}
