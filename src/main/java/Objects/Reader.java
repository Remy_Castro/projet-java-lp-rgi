package Objects;

import java.io.Serializable;

public class Reader implements Serializable {
    private Integer ID;
    private String lastname;
    private String firstname;

    public Reader(Integer ID, String lastname, String firstname) {
        this.ID = ID;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public Reader(String lastname, String firstname) {
        this.ID = 0;
        this.lastname = lastname;
        this.firstname = firstname;
    }

    public Integer getID() {
        return ID;
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    @Override
    public String toString() {
        return "Reader{" +
                "ID=" + ID +
                ", lastname='" + lastname + '\'' +
                ", firstname='" + firstname + '\'' +
                '}';
    }
}
