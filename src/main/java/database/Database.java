package database;

import Objects.Book;
import Objects.Reader;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

public class Database {
    private String urlJDBC;
    private Connection cnx;
    static private Statement stmt;
    static public ArrayList<Book> allBooks = new ArrayList<>();
    static public ArrayList<Reader> allReaders = new ArrayList<>();


    public Database() {
        this.urlJDBC = "jdbc:mysql://localhost:3306/remy_cas_java";

        this.cnx = null;
        this.stmt = null;

        try {
            //Connexion à la base de données
            this.cnx = DriverManager.getConnection(this.urlJDBC,"root","root");

            //Création statement
            this.stmt = this.cnx.createStatement();
        } catch (SQLException e ) {
            System.out.println("Erreur lors de l'initialisation de la base de données.");
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public void init(){
        this.createTables();
        this.insertData();
    }

    /**
     * Récupération de tous les livres
     * @return une liste de livres
     */
    static public ArrayList<Book> getAllBooks() {
        String req = "SELECT * from livres;";

        ResultSet result = null;

        try {
            result = stmt.executeQuery(req);
            allBooks.clear(); //On vide l'ancien


            while (result.next()) {
                Integer id = result.getInt("ID");
                String titre = result.getString("Titre");
                Integer tome = result.getInt("Tome");
                String auteur = result.getString("Auteur");

                allBooks.add(new Book(id, titre, tome, auteur));
            }

            result.close();
        }
        catch(SQLException e) {
            System.out.println("Erreur lors de la récupération des livres.");
            e.printStackTrace();
            System.exit(-1);
        }

        return allBooks;
    }

    /**
     * Récupération de tous les lecteurs
     * @return une liste de livres
     */
    static public ArrayList<Reader> getAllReaders() {
        String req = "SELECT * from lecteurs;";

        ResultSet result = null;

        try {
            result = stmt.executeQuery(req);
            allReaders.clear(); //On vide l'ancien


            while (result.next()) {
                Integer id = result.getInt("ID");
                String lastname = result.getString("Nom");
                String firstname = result.getString("Prenom");
                allReaders.add(new Reader(id, lastname, firstname));
            }

            result.close();
        }
        catch(SQLException e) {
            System.out.println("Erreur lors de la récupération des livres.");
            e.printStackTrace();
            System.exit(-1);
        }

        return allReaders;
    }

    /**
     * Récupération des livres d'un utilisateur
     * @param userId
     * @return une liste une livres
     */
    static public ArrayList<Book> getUserBooks(Integer userId) {
        ArrayList<Book> userBooks = new ArrayList<>();

        String req = "SELECT ID_livre from reservation where ID_lecteur = " +userId;

        ResultSet result = null;

        try {
            result = stmt.executeQuery(req);
            userBooks.clear();
            while(result.next()) {
                //On récupère l'id du livre possédé
                Integer id = result.getInt("ID_livre");

                //On recherche le livre
                Optional<Book> user_book = allBooks.stream()
                        .filter(book -> book.getID() == id)
                        .findAny();

                //On ajoute le livre dans la liste de l'utilisateur
                if(user_book.isPresent()){
                    userBooks.add(user_book.get());
                }
            }
        }
        catch (SQLException e) {
            System.out.println("Erreur lors de la récupération des livres de l'utilisateur");
            e.printStackTrace();
            System.exit(-1);
        }

        return userBooks;

    }

    /**
     * Enregistre un livre en base de données
     * @param titre
     * @param tome
     * @param author
     * @return booléen qui indique si le livre a été crée
     */
    static public Integer addBook(String titre,Integer tome, String author){
        Integer max_id;
        //Récupère l'id max en base de données
        String idMax = "select MAX(ID) as 'max' from livres";

        String req = "insert into livres (Titre, Tome, Auteur) VALUES " +
                "('"+titre+"','"+tome+"','"+author+"');";

        String req_doublon = "select count(ID) as 'doublon' from livres where Titre='"+titre+"' and Tome='"+tome+"' and Auteur='"+author+"';";

        ResultSet result_doublon = null;
        ResultSet result = null;
        try {
            result_doublon = stmt.executeQuery(req_doublon);

            //On vérifie si le livre n'existe pas déjà en BDD
            if(result_doublon.next()){
                if(result_doublon.getInt("doublon") > 0){
                    return -1;
                }
                else {
                    //Id max stocké en base de données
                    result = stmt.executeQuery(idMax);

                    //Si on peut récupérer l'id
                    if(result.next()) {
                        max_id = result.getInt("max");

                        //Insertion du nouveau livre
                        stmt.executeUpdate(req);
                        allBooks.add(new Book(max_id+1, titre,tome,author));

                        return max_id;
                    }
                }
            }
        }
        catch(SQLException e){
            System.out.println("Erreur lors de la création du livre");
            e.printStackTrace();
            System.exit(-1);

        }
        return -1;
    }

    static public Integer addReader(String firstname, String lastname){
        Integer max_id;

        //Récupère l'id max en base de données
        String idMax = "select MAX(ID) as 'max' from lecteurs";


        String req = "insert into lecteurs (Prenom,Nom) VALUES" +
                "('"+firstname+"','"+lastname+"');";

        String req_doublon = "select count(ID) as 'doublon' from lecteurs where Prenom='"+firstname+"' and Nom='"+lastname+"';";

        ResultSet result_doublon = null;
        ResultSet result = null;

        try {
            result_doublon = stmt.executeQuery(req_doublon);
            if(result_doublon.next()){
                if(result_doublon.getInt("doublon") > 0){
                    System.out.println("L'utilisateur existe déjà");
                    return -1;
                }
                else {
                    //Id max stocké en base de données
                    result = stmt.executeQuery(idMax);

                    if(result.next()){
                        max_id = result.getInt("max");

                        //Insertion du nouveau livre
                        stmt.executeUpdate(req);
                        allReaders.add(new Reader(max_id+1,lastname,firstname));
                        return max_id;
                    }
                }
            }
        }
        catch(SQLException e){
            System.out.println("Erreur lors de la création du lecteur");
            e.printStackTrace();
            System.exit(-1);
        }
        return -1;
    }

    /**
     * Sauvegarde en base de données la réservation d'un livre par l'utilisateur
     * @param book
     * @return booléen qui indique si le livre a été réservé
     */
    static public boolean takeBook(Reader reader, Book book){
        //Récupération de l'id de l'utilisateur -> variable statique initialisée à la connexion
        //Qd on se connecte, récupoère l'id en fonction de l'username / mot de passe et User.setId(id)

        String req = "insert into reservation (ID_livre, ID_lecteur) VALUES ("+book.getID()+","+reader.getID()+");";

        String req_doublon = "select count(ID) as 'doublon' from reservation where  ID_livre = " + book.getID() + " and ID_lecteur="+reader.getID();

        ResultSet result = null;
        try {
            result = stmt.executeQuery(req_doublon);

            if(result.next()){
                if(result.getInt("doublon") > 0){
                    //L'utilisateur possède déjà le livre
                    System.out.println("Vous possédez déjà ce livre");
                    return false;
                }
                else {
                    //L'utilisateur ne possède pas le livre
                    stmt.executeUpdate(req);
                    return true;
                }
            }
        }
        catch(SQLException e){
            System.out.println("Erreur lors de la réservation du livre");
            e.printStackTrace();
            System.exit(-1);
        }

        return false;
    }

    /**
     * L'utilisateur rend un livre
     * @param book
     */
    static public boolean returnBook(Reader reader,Book book){
        String req = "delete from reservation where ID_livre = "+book.getID()+" and ID_lecteur ="+reader.getID();

        try {
            stmt.executeUpdate(req);
            return true;
        }
        catch(SQLException e){
            System.out.println("Erreur lors de la suppression du livre");
            e.printStackTrace();
            System.exit(-1);
        }
        return false;
    }


    /**
     * Création des tables requises à l'application
     *  - livres
     *  - utilisateurs
     *  - reservation
     * ON UPDATE CASCADE, ON DELETE CASCADE :
     *  À la mise à jour/suppression d'un livre ou utilisateur -> modifie la table reservation
     * @return
     */
    private void createTables() {
        String reqLivres =
                "CREATE TABLE IF NOT EXISTS livres (" +
                    "ID INT AUTO_INCREMENT PRIMARY KEY," +
                    "Titre VARCHAR(50)NOT NULL," +
                    "Tome INT NOT NULL," +
                    "Auteur VARCHAR(50)NOT NULL" +
                ")ENGINE=InnoDB;";

        String reqLecteurs =
                "CREATE TABLE IF NOT EXISTS lecteurs(" +
                    "ID INT AUTO_INCREMENT PRIMARY KEY," +
                    "Nom VARCHAR(50) NOT NULL," +
                    "Prenom varchar(50) NOT NULL" +
                ")ENGINE=InnoDB;";

        String reqReservation =
                "CREATE TABLE IF NOT EXISTS reservation (" +
                        "ID INT AUTO_INCREMENT PRIMARY KEY," +
                        "ID_livre INT NOT NULL," +
                        "ID_lecteur INT NOT NULL," +
                        "INDEX livre_id (ID_livre)," +
                        "INDEX lecteur_id (ID_lecteur)," +
                        "FOREIGN KEY (ID_livre) REFERENCES livres(ID) ON DELETE CASCADE ON UPDATE CASCADE," +
                        "FOREIGN KEY (ID_lecteur) REFERENCES lecteurs(ID) ON DELETE CASCADE ON UPDATE CASCADE" +
                ")ENGINE=InnoDB;";
        try {
            //Création des tables
            this.stmt.executeUpdate(reqLivres);
            this.stmt.executeUpdate(reqLecteurs);
            this.stmt.executeUpdate(reqReservation);
        }
        catch(SQLException e) {
            System.out.println("Échec lors de la création des tables");
            e.printStackTrace();
            System.exit(-1);

        }
    }

    /**
     * Données de test
     */
    private void insertData(){
        String reqBooks = "insert ignore into livres (ID,Titre,Tome,Auteur) VALUES" +
                "(1,'Harry potter',1,'JK Rowling')," +
                "(2,'Harry potter',2,'JK Rowling')," +
                "(3,'Harry potter',3,'JK Rowling')," +
                "(4,'Harry potter',4,'JK Rowling')," +
                "(5,'Harry potter',5,'JK Rowling')," +
                "(6,'Harry potter',6,'JK Rowling');";
        String reqReaders = "insert ignore into lecteurs (ID,Nom,Prenom) VALUES" +
                "(1,'Castro','Remy')," +
                "(2,'Doe','John')," +
                "(3,'Luch','Romain');";
        try {
            //Création des tables
            this.stmt.executeUpdate(reqBooks);
            this.stmt.executeUpdate(reqReaders);
        }
        catch(SQLException e) {
            System.out.println("Échec lors de l'insertion des données de test");
            e.printStackTrace();
            System.exit(-1);

        }
    }
}
