package Server;

import Objects.Book;
import Objects.Reader;
import database.Database;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

public class ServerThread implements Runnable{
    private Thread _t; // contiendra le thread du client
    private Socket _s; // recevra le socket lié au client
    private Server _server;
    private Boolean dataImported; //Permet de savoir si les données ont été importées
    static private ObjectOutputStream os = null;


    public ServerThread(Socket s,Server server) {
        this._s = s;
        this._server = server;
        this._server.addClient(); //Ajoute un client
        this.dataImported = false;

        Database db = new Database();
        db.init();
        try {
            os = new ObjectOutputStream(_s.getOutputStream());

            //Chargement de toutes les données : livres et lecteurs
            if(!dataImported){
                this.getAllBooks();
                this.getAllReaders();
                dataImported=true;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        _t = new Thread(this); // instanciation du thread
        _t.start(); // démarrage du thread, la fonction run() est lancée


    }

    @Override
    public void run() {
        boolean exit = false;
        System.out.println("Un client s'est connecté au serveur");
        System.out.println("Nombre de client : " + this._server.getNoClients());

        ObjectInputStream is = null;
        try {
            String operation = null;
            try {
                is = new ObjectInputStream(_s.getInputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                while(true){
                    operation = is.readUTF();

                    //Différentes opérations possibles
                    switch(operation){
                        case "allReaders":
                            this.getAllReaders();
                            break;
                        case "addBook":
                            Book book = (Book) is.readObject();
                            this.addBook(book);
                            break;
                        case "addReader":
                            Reader reader = (Reader) is.readObject();
                            this.addReader(reader);
                            break;
                        case "getUserBooks":
                            Integer id = is.readInt();
                            this.getUserBooks(id);
                            break;
                        case "addReaderBook":
                            reader = (Reader) is.readObject();
                            book = (Book) is.readObject();
                            this.addReaderBook(reader,book);
                            break;
                        case "returnBook":
                            reader = (Reader) is.readObject();
                            book = (Book) is.readObject();
                            this.returnBook(reader,book);
                            break;
                        case "disconnect":
                            exit=true;
                            _server.delClient();
                            System.out.println("Un client s'est déconnecté au serveur");
                            System.out.println("Nombre de client : " + this._server.getNoClients());
                            try
                            {
                                this._s.close(); //Fermeture du socket
                            }
                            catch (IOException e){ }
                    }
                    if(exit){
                        //Déconnexion de l'utilisateur -> sort du while
                        break;
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    /**
     * Nouveau livre
     * @param book
     */
    private void addBook(Book book){
        Integer r = Database.addBook(book.getTitre(),book.getTome(),book.getAuteur());
        if(r > -1){
            try {
                os.writeBoolean(true);
                os.writeInt(r+1);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                os.writeBoolean(false);
                os.writeInt(r+1);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Nouveau lecteur
     * @param reader
     */
    private void addReader(Reader reader){
        Integer r = Database.addReader(reader.getFirstname(), reader.getLastname());

        if(r > -1){
            try {
                os.writeBoolean(true);
                os.writeInt(r+1);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                os.writeBoolean(false);
                os.writeInt(r+1);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Récupère tous les livres de la librairie
     */
    private void getAllBooks(){
        ArrayList<Book> books;
        books = Database.getAllBooks();

        try {
            os.writeObject(books);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupère tous les lecteurs de la librairie
     */
    private void getAllReaders(){
        ArrayList<Reader> readers;
        readers = Database.getAllReaders();

        try {
            os.writeObject(readers);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * Liste des livres d'un lecteur
     * @param id : id du lecteur
     */
    private void getUserBooks(Integer id){
        try {
            ArrayList<Book> books = Database.getUserBooks(id);
            os.writeObject(books);
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Un lecteur prend un livre à la librairie
     * @param reader
     * @param book
     */
    private void addReaderBook(Reader reader, Book book){
        if(Database.takeBook(reader, book)){
            try {
                os.writeBoolean(true);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                os.writeBoolean(false);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Un lecteur redonne un livre emprunté à la librairie
     * @param reader
     * @param book
     */
    private void returnBook(Reader reader, Book book){
        if(Database.returnBook(reader, book)){
            try {
                os.writeBoolean(true);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                os.writeBoolean(false);
                os.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
