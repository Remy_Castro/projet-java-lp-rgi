package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    static private int noClients=0;
    public static void main(String[] args) {
        Server server = new Server();
        int port = 1234;

        ServerSocket socketServer = null;
        Socket socket = null;

        try {
            socketServer = new ServerSocket(port);
            System.out.println("Socker server created");

            while(true){ //En attente de connexion en boucle
                new ServerThread(socketServer.accept(),server);
            }

        }
        catch(IOException e){
            System.out.println("Erreur lors de la création du serveur");
            e.printStackTrace();
            System.exit(-2);

        }

    }

    /**
     * Récupère le nombre de client connecté au serveur
     * @return
     */
    synchronized public int getNoClients() {
        return noClients;
    }

    /**
     * Un client se connecte au serveur
     */
    synchronized public void addClient()
    {
        this.noClients++;
    }

    /**
     * Un client se déconnecte du serveur
     */
    synchronized public void delClient()
    {
        this.noClients--;
    }
}
